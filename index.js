	

	//3. Create a variable number that will store the value of the number provided by the user via the prompt.

let number = Number(prompt("Give me a number"));
console.log(`The number you provided ${number}`)

	//4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
	
	for(let i = 0; i <= 0; number--){

	//5. Create a condition that if the current value is less than or equal to 50, stop the loop.

		if(number <= 50){
			console.log("The current value is at 50. Terminating the loop.")
			break;
	}

	//6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
		
		if(number % 10 === 0){
			console.log("The number is divisible by 10. Skipping the number.")
			continue;
	}

	//7. Create another condition that if the current value is divisible by 5, print the number.
		
		if(number % 5 === 0){
			console.log(number);
			continue;
	}

};





	//8. Create a variable that will contain the string supercalifragilisticexpialidocious.

let string = "supercalifragilisticexpialidocious"
console.log(string);

	//9. Create another variable that will store the consonants from the string.

let filteredString = ""

	//10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
	
	for(let i = 0; i < string.length; i++){
		
	//11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
		
		if (
			string[i].toLowerCase() == "a" ||
			string[i].toLowerCase() == "e" ||
			string[i].toLowerCase() == "i" ||
			string[i].toLowerCase() == "o" ||
			string[i].toLowerCase() == "u") continue; 

		//12. Create an else statement that will add the letter to the second variable.	
			
			filteredString += string[i];
		} 
		console.log(filteredString)




